# web-clock-8001

Simple web render for [Clock 8001](https://gitlab.com/Depili/clock-8001)

Work in progress, at this point only basic clock (source1) is somehow supported.


## usage

* ( install golang: https://golang.org/doc/install )
* clone repo to go directory
* go run web-clock-8001.go
* open localhost:8800/textclock.html or roundclock.html
* edit css in html-directory as needed


## TODO

* fix leds in round clock and update struct for curren clock api
* add text/icon support
* add selection for multiple sources

