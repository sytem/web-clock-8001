package main

import (
	"log"
	"net/http"
	"strings"
	"strconv"

	"github.com/gorilla/websocket"
	"gitlab.com/Depili/go-osc/osc"
)

var clients = make(map[*websocket.Conn]bool) // connected clients
var broadcast = make(chan [4]Sourcedata)     // broadcast channel

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

//new messagetype for V4

type Sourcedata struct {
UUID string `json:"uuid"`
Hidden bool `json:"hidden"`
Output string `json:"output"`
Compact string `json:"compact"`
Icon string `json:"icon"`
Progress float32 `json:"progress"`
Expired bool `json:"expired"`
Paused bool `json:"paused"`
Title string `json:"title"`
Mode int32 `json:"mode"`
}

var msg [4]Sourcedata


func main() {
	//osc listen,
	addr := "0.0.0.0:1245"

	//files for html-page display
	fs := http.FileServer(http.Dir("html"))
	http.Handle("/", fs)

	//websocket for live data
	http.HandleFunc("/ws", handleConnections)
	go handleMessages()

	server := &osc.Server{Addr: addr}

	server.Handle("/clock/source/", func(oscMsg *osc.Message) {
		//osc.PrintMessage(oscMsg)
		//log.Println(oscMsg.Address)

		// new magic here

		sourceString := strings.SplitN(oscMsg.Address, "/",5)[3]
		sourceNumber, err := strconv.Atoi(sourceString)
		if err != nil {
			panic(err)
		}

		// check if we have data for address, send and clear (we have seen all sources...)

		if msg[sourceNumber-1].UUID != "" {

			broadcast <- msg

			//log.Println("tick")

			// clear
			msg[0].UUID = ""
			msg[1].UUID = ""
			msg[2].UUID = ""
			msg[3].UUID = ""
		}

		//fill message with current data

		msg[sourceNumber-1].UUID = oscMsg.Arguments[0].(string)
		msg[sourceNumber-1].Hidden = oscMsg.Arguments[1].(bool)
		msg[sourceNumber-1].Output = oscMsg.Arguments[2].(string)
		msg[sourceNumber-1].Compact = oscMsg.Arguments[3].(string)
		msg[sourceNumber-1].Icon = oscMsg.Arguments[4].(string)
		msg[sourceNumber-1].Progress = oscMsg.Arguments[5].(float32)
		msg[sourceNumber-1].Expired = oscMsg.Arguments[6].(bool)
		msg[sourceNumber-1].Paused = oscMsg.Arguments[7].(bool)
		msg[sourceNumber-1].Title = oscMsg.Arguments[8].(string)
		msg[sourceNumber-1].Mode = oscMsg.Arguments[9].(int32)

	})



	go httpServerStart()
	log.Println("http server started on :8800")

  // check if address is in use, mac has broblem with this
	err := server.ListenAndServe()
	if err != nil {
		panic(err)
	}

	log.Println("OSC server started")

}

func httpServerStart() {
	log.Println("http server start")
	err42 := http.ListenAndServe(":8800", nil)
	if err42 != nil {
		panic(err42)
	}
}

func handleConnections(w http.ResponseWriter, r *http.Request) {
	// Upgrade initial GET request to a websocket
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("New connection \n")
	// Make sure we close the connection when the function returns
	//defer ws.Close()

	// Register our new client
	clients[ws] = true
}

func handleMessages() {
	for {
		// Grab the next message from the broadcast channel
		message := <-broadcast
		// Send it out to every client that is currently connected
		for client := range clients {
			//log.Println(message[3].output)
			err := client.WriteJSON(message)
			if err != nil {
				log.Printf("error: %v", err)
				client.Close()
				delete(clients, client)
			}
		}
	}
}
