module gitlab.com/sytem/web-clock-8001

go 1.16

require (
	github.com/gorilla/websocket v1.4.2
	gitlab.com/Depili/go-osc v0.0.0-20210208185228-dd2b4c8eac31
)
