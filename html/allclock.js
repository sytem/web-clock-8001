// webclock for clock8001, Teppo Rekola 2021

// update data
function clock(msg) {
  for (let i = 0; i < msg.length; i++) {
    let elementname = "source" + (i+1);
    var element = document.getElementById(elementname);

    if (msg[i].title == "") {
      msg[i].title = elementname;
    }
    element.innerHTML = "<span class='title'>" + msg[i].title +": </span>";

    if (!msg[i].hidden) {
       element.innerHTML += "<span class='icon'>" + msg[i].icon +
                          " </span><span class='output'>" + msg[i].output +
                          "</span>";
      }

  }


}

// connection closed, stop everythig
function clearClock() {
  source1.innerHTML = "<span class='title'>No data from server</span;"
  source2.innerHTML = "";
  source3.innerHTML = "";
  source4.innerHTML = "";
}
