// webclock for clock8001, Teppo Rekola 2021

function runClock() {
  var status = document.getElementById("status");
	var debug  = document.getElementById("debug");

	var socket = new WebSocket("ws://" + location.host + "/ws");

	socket.onopen = function () {
		status.innerHTML += "Status: Connected to websocket server\n";
	};

  socket.onmessage = function (e) {
		//debug.innerHTML = "Server: " + e.data + "\n";
    var msg = JSON.parse(e.data)
		clock (msg);
		};

  socket.onclose = function(e) {
    status.innerHTML = "Status: websocket connection closed, check server\n";
    clearClock();
    };

}
